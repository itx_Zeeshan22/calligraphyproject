import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpConfigInterceptor, SpinnerHttpInterceptor, } from './app.interceptors';

const httpInterceptProviders = [{
    provide: HTTP_INTERCEPTORS,
    useClass: HttpConfigInterceptor,
    multi: true
},
    {
        provide: HTTP_INTERCEPTORS,
        useClass: SpinnerHttpInterceptor,
        multi: true,
    }
]

export default httpInterceptProviders
