import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {finalize, map, tap} from "rxjs/operators";
import {Observable} from "rxjs";
import {Inject, Injectable} from "@angular/core";
import {NgxSpinnerService} from "ngx-spinner";


@Injectable()
export class SpinnerHttpInterceptor implements HttpInterceptor {

    count = 0;

    constructor(private spinner: NgxSpinnerService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this.beginRequest();
        return next.handle(req).pipe(
            finalize(() => {
                this.endRequest();
            })
        );
    }

    beginRequest() {
        this.count = Math.max(this.count, 0) + 1;

        if (this.count === 1) {
            // this.abns.busy.next(true);
            this.spinner.show()
        }
    }

    endRequest() {
        this.count = Math.max(this.count, 1) - 1;

        if (this.count === 0) {
            // this.abns.busy.next(false);
            this.spinner.hide()
        }
    }

}

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = localStorage.getItem('token');

        if (token) {
            request = request.clone({headers: request.headers.set('Authorization', `Bearer ${token}`)});
        }

        request = request.clone({headers: request.headers.set('Accept', 'application/json')});

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('event--->>>', event);
                }
                return event;
            }));
    }
}
