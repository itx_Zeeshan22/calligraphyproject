import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';


export interface IUploadFile {
    'Id': number;
    'MasterId': number;
    'FileToUpload': 'string';
}

import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class ApiService {
    url = 'http://192.168.0.123:81/';

    constructor(private http: HttpClient) {
    }

    addAbout(data): Observable<any> {
        return this.http.put(this.url + 'api/About/Update', data);
    }

    getAbout(): Observable<any> {
        return this.http.get(this.url + 'api/About/List');
    }

    addCategory(addNewCategory: any): Observable<any> {
        return this.http.post(this.url + 'api/Category/Save', addNewCategory);
    }

    getCategory(): Observable<any> {
        return this.http.get(this.url + 'api/Category/List');
    }

    deleteCategory(id): Observable<any> {
        return this.http.delete(this.url + `api/Category/Delete/${id}`);
    }

    getImages(): Observable<any> {
        return this.http.get(this.url + 'api/ImageGallary/List');
    }

    deleteImage(id: any): Observable<any> {
        return this.http.delete(this.url + `api/ImageGallary/Delete/${id}`);

    }

    getByCategory(id: any): Observable<any> {
        return this.http.get(this.url + `api/ImageGallary/Get/${id}`);
    }

    saveImages(data): Observable<any> {
        return this.http.post(this.url + 'api/ImageGallary', data);
    }
}
