import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app.routing';
import {AppComponent} from './app.component';
import {ViewsModule} from './views/views.module';
import {TranslateModule} from '@ngx-translate/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import {HttpClientModule} from '@angular/common/http';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {LayoutContainersModule} from './containers/layout/layout.containers.module';
import {JwtModule} from '@auth0/angular-jwt';
import {AuthService} from './shared/auth.service';
import {AccordionModule} from 'ngx-bootstrap/accordion';
import {BsModalService} from 'ngx-bootstrap/modal';
import httpInterceptProviders from './interceptor';
import {NgxSpinnersModule} from 'ngx-spinners';
import {SelectDropDownModule} from 'ngx-select-dropdown';

// import {SimpleNotificationsModule} from 'angular2-notifications';
import {ContextMenuModule} from 'ngx-contextmenu';
import {FileUploadModule} from 'ng2-file-upload';
import {NgxSpinnerModule} from 'ngx-spinner';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {OWL_DATE_TIME_FORMATS, OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {DatePipe} from '@angular/common';
import {CarouselModule} from 'ngx-bootstrap/carousel';
import {SharedModule} from './shared1/shared.module';
import {LayoutsModule} from "./layouts/layouts.module";
import {ScrollToModule} from "ng2-scroll-to-el";
export const MY_NATIVE_FORMATS = {
    fullPickerInput: {year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric'},
    datePickerInput: {year: 'numeric', month: 'numeric', day: 'numeric'},
    timePickerInput: {hour: 'numeric', minute: 'numeric'},
    monthYearLabel: {year: 'numeric', month: 'short'},
    dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
    monthYearA11yLabel: {year: 'numeric', month: 'long'},
};

@NgModule({
    imports: [
        BrowserModule,
        ViewsModule,
        AppRoutingModule,
        LayoutContainersModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgxSpinnersModule,
        SelectDropDownModule,
        LayoutsModule,
        ScrollToModule,
        CarouselModule,
        SharedModule,
        FileUploadModule,
        ContextMenuModule.forRoot(),
        TranslateModule.forRoot(),
        // SimpleNotificationsModule.forRoot({
        //         position: ['bottom', 'left'],
        //         showProgressBar : false
        //     }
        // ),
        AngularFireModule.initializeApp(environment.firebase),
        AccordionModule.forRoot(),
        JwtModule.forRoot({
            config: {
                tokenGetter: AuthService.tokenGetter,
                authScheme: 'Token '
            }
        }),
        NgxSpinnerModule,
        SimpleNotificationsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        ConfirmationPopoverModule.forRoot({
      focusButton: 'confirm',
    })
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        DatePipe,
        {provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS},
        httpInterceptProviders,
        BsModalService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
