import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortfolioMetroComponent } from './portfolio-metro/portfolio-metro.component';

const routes: Routes = [
  {
    path: '',
    component: PortfolioMetroComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule { }
