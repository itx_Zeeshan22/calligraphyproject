import { Component, OnInit } from '@angular/core';
import {PortfolioService} from '../portfolio.service';

@Component({
  selector: 'app-portfolio-footer',
  templateUrl: './portfolio-footer.component.html',
  styleUrls: ['./portfolio-footer.component.scss']
})
export class PortfolioFooterComponent implements OnInit {
  userdetail: any;
  userdata: any;
  i: any;

  constructor(private api: PortfolioService) { }


  ngOnInit() {
    this.getdetail();
  }

  getdetail() {
        this.api.getdetail().subscribe(data => {
            this.userdetail = data.data;
            console.log(this.userdetail);
            for (this.i = 0; this.i < 2; this.i++)
            {
                if (this.i === 0)
                {
                    this.userdata = this.userdetail[this.i];
                    console.log(this.userdata);
                }

            }
        }, error => {
            console.log(error.message);
        });
    }

}
