import { Component, OnInit } from '@angular/core';
import {
  ButtonsConfig,
  ButtonsStrategy,
  AdvancedLayout,
  Image,
  KS_DEFAULT_BTN_CLOSE,
  KS_DEFAULT_BTN_DOWNLOAD,
  KS_DEFAULT_BTN_FULL_SCREEN,
  PlainGalleryConfig,
  PlainGalleryStrategy,
} from '@ks89/angular-modal-gallery';
import {PortfolioService} from '../portfolio.service';

@Component({
  selector: 'app-portfolio-gallery',
  templateUrl: './portfolio-gallery.component.html',
  styleUrls: ['./portfolio-gallery.component.scss']
})
export class PortfolioGalleryComponent implements OnInit {

  constructor(private api: PortfolioService) {
  }

  public customizer: any = 'all';
    allImages: Image[] = [];
  // allImages: Image[] = [
  //   // new Image(0, { img: 'assets/images/portfolio/1.jpg' }),
  //   // new Image(0, { img: 'assets/images/portfolio/2.jpg' }),
  //   // new Image(1, { img: 'assets/images/portfolio/3.jpg' }),
  //   // new Image(2, { img: 'assets/images/portfolio/5.jpg' }),
  //   // new Image(3, { img: 'assets/images/portfolio/4.jpg' }),
  //   // new Image(4, { img: 'assets/images/portfolio/6.jpg' }),
  //   // new Image(5, { img: 'assets/images/portfolio/7.jpg' }),
  //   // new Image(6, { img: 'assets/images/portfolio/8.png' }),
  //   // new Image(7, { img: 'assets/images/portfolio/9.jpg' }),
  // ];

  buttonsConfigCustom: ButtonsConfig = {
    visible: true,
    strategy: ButtonsStrategy.CUSTOM,
    buttons: [
      // KS_DEFAULT_BTN_FULL_SCREEN,
      // KS_DEFAULT_BTN_DOWNLOAD,
      KS_DEFAULT_BTN_CLOSE
    ]
  };

  customPlainGalleryRowDescConfig: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.CUSTOM,
    layout: new AdvancedLayout(-1, true)
  };

  i: number;
  j: number;
  catgories: any;
  catgories1: any;
  catgories2: any;
  catgories3: any;
  catgories4: any;
  totalimages: any;
  shoesImages: Image[] = [];
  watchImages: Image[] = [];
  fashionImages: Image[] = [];
  bagImages: Image[] = [];

  openImageModalRowDescription(image: Image) {
    const index: number = this.getCurrentIndexCustomLayout(image, this.allImages);
    console.log(index);
    this.customPlainGalleryRowDescConfig = Object.assign({}, this.customPlainGalleryRowDescConfig, { layout: new AdvancedLayout(index, true) });
    console.log(this.customPlainGalleryRowDescConfig);
  }

  private getCurrentIndexCustomLayout(image: Image, images: Image[]): number {
      console.log('here Image');
      console.log(image);
      console.log(images);
      return image ? images.indexOf(image) : -1;
  }
  openGallery(val) {
    this.customizer = val;
    console.log(this.customizer);
  }

  ngOnInit() {
    this.getAllImages();
    this.getAllCatagories();
  }

  getAllImages() {
        this.api.getimage().subscribe(data => {
            this.totalimages = data.data;
            console.log(this.totalimages);
            for (this.j = 0; this.j < this.totalimages.length; this.j++)
            {
                this.allImages.push(new Image(this.j, { img: this.totalimages[this.j].imageUrl }));
            }
            console.log('array of images');
            console.log(this.allImages);
        }, error => {
            console.log(error.message);
        });
    }

  getAllCatagories() {
        this.api.getcatagories().subscribe(data => {
            this.catgories = data.data;
            console.log(this.catgories);
            for (this.i = 0; this.i < 5; this.i++)
            {
                if (this.i === 0)
                {
                  this.catgories1 = this.catgories[this.i];
                  console.log(this.catgories1);
                  this.api.getuniqueimages(this.catgories1.id).subscribe(data1 => {
                        this.totalimages = data1.data;
                        console.log(this.totalimages);
                        for (this.j = 0; this.j < this.totalimages.length; this.j++)
                        {
                            this.fashionImages.push(new Image(this.j, { img: this.totalimages[this.j].imageUrl }));
                        }
                        console.log('fashionImages');
                        console.log(this.fashionImages);

                    }, error => {
                        console.log(error.message);
                    });
                }
                if (this.i === 1)
                {
                  this.catgories2 = this.catgories[this.i];
                  console.log(this.catgories2);
                  this.api.getuniqueimages(this.catgories2.id).subscribe(data1 => {
                        this.totalimages = data1.data;
                        console.log(this.totalimages);
                        for (this.j = 0; this.j < this.totalimages.length; this.j++)
                        {
                            this.bagImages.push(new Image(this.j, { img: this.totalimages[this.j].imageUrl }));
                        }
                        console.log('bagImages');
                        console.log(this.bagImages);

                    }, error => {
                        console.log(error.message);
                    });
                }
                if (this.i === 2)
                {
                  this.catgories3 = this.catgories[this.i];
                  console.log(this.catgories3);
                  this.api.getuniqueimages(this.catgories3.id).subscribe(data1 => {
                        this.totalimages = data1.data;
                        console.log(this.totalimages);
                        for (this.j = 0; this.j < this.totalimages.length; this.j++)
                        {
                            this.shoesImages.push(new Image(this.j, { img: this.totalimages[this.j].imageUrl }));
                        }
                        console.log('shoesImages');
                        console.log(this.shoesImages);

                    }, error => {
                        console.log(error.message);
                    });
                }
                if (this.i === 3)
                {
                   this.catgories4 = this.catgories[this.i];
                   console.log(this.catgories4);
                   this.api.getuniqueimages(this.catgories4.id).subscribe(data1 => {
                        this.totalimages = data1.data;
                        console.log(this.totalimages);
                        for (this.j = 0; this.j < this.totalimages.length; this.j++)
                        {
                            this.watchImages.push(new Image(this.j, { img: this.totalimages[this.j].imageUrl }));
                        }
                        console.log('watchImages');
                        console.log(this.watchImages);

                    }, error => {
                        console.log(error.message);
                    });
                }
            }
        }, error => {
            console.log(error.message);
        });
    }

}
