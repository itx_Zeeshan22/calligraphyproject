import { Component, OnInit } from '@angular/core';
import {
  ButtonsConfig,
  ButtonsStrategy,
  AdvancedLayout,
  Image,
  KS_DEFAULT_BTN_CLOSE,
  KS_DEFAULT_BTN_DELETE,
  KS_DEFAULT_BTN_DOWNLOAD,
  KS_DEFAULT_BTN_EXTURL,
  KS_DEFAULT_BTN_FULL_SCREEN,
  PlainGalleryConfig,
  PlainGalleryStrategy,
} from '@ks89/angular-modal-gallery';
import {PortfolioService} from '../../portfolio.service';
import {PortfolioGalleryComponent} from '../portfolio-gallery.component';

@Component({
  selector: 'app-bags',
  templateUrl: './bags.component.html',
  styleUrls: ['./bags.component.scss']
})
export class BagsComponent implements OnInit {

  constructor(private api: PortfolioService, private catagory: PortfolioGalleryComponent) { }
;
  // catagoryId: number;
  // totalimages: any;
  // j: number;

  bagImages: Image[] = [];

  // bagImages: Image[] = [
  //   new Image(0, { img: 'assets/images/portfolio/1.jpg' }),
  //   new Image(5, { img: 'assets/images/portfolio/6.jpg' }),
  //   new Image(6, { img: 'assets/images/portfolio/7.jpg' }),
  //   new Image(7, { img: 'assets/images/portfolio/8.png' })
  // ];

  buttonsConfigCustom: ButtonsConfig = {
    visible: true,
    strategy: ButtonsStrategy.CUSTOM,
    buttons: [
      // KS_DEFAULT_BTN_FULL_SCREEN,
      // KS_DEFAULT_BTN_DOWNLOAD,
      KS_DEFAULT_BTN_CLOSE
    ]
  };

  customPlainGalleryRowDescConfig: PlainGalleryConfig = {
    strategy: PlainGalleryStrategy.CUSTOM,
    layout: new AdvancedLayout(-1, true)
  };

  ngOnInit() {
    this.bagImages = this.catagory.bagImages;
    // this.getAllImages();
  }

  openImageModalRowDescription(image: Image) {
    const index: number = this.getCurrentIndexCustomLayout(image, this.bagImages);
      // tslint:disable-next-line:max-line-length
    this.customPlainGalleryRowDescConfig = Object.assign({}, this.customPlainGalleryRowDescConfig, { layout: new AdvancedLayout(index, true) });
  }

  private getCurrentIndexCustomLayout(image: Image, images: Image[]): number {
    return image ? images.indexOf(image) : -1;
  }

  // getAllImages() {
  //       console.log(this.catagoryId);
  //       this.api.getuniqueimages(this.catagoryId).subscribe(data => {
  //           this.totalimages = data.data;
  //           console.log(this.totalimages);
  //           for(this.j = 0; this.j < this.totalimages.length; this.j++)
  //           {
  //               this.bagImages.push(new Image(this.j, { img: this.totalimages[this.j].imageUrl }));
  //           }
  //           console.log('array of images');
  //           console.log(this.bagImages);
  //       }, error => {
  //           console.log(error.message);
  //       });
  //   }

}
