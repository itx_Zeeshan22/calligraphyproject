import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  constructor(private http: HttpClient) { }

  url = 'http://192.168.0.123:81';

  getimage(): Observable<any> {
      return this.http.get(this.url + '/api/ImageGallary/List');
  }

  getuniqueimages(id: number): Observable<any> {
      return this.http.get(this.url + `/api/ImageGallary/Get/${id}`);
  }

  getdetail(): Observable<any> {
      return this.http.get(this.url + '/api/About/List');
  }

  getcatagories(): Observable<any> {
      return this.http.get(this.url + '/api/Category/List');
  }


}
