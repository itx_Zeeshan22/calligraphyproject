import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {NotificationsService, NotificationType} from 'angular2-notifications';
import {Router} from '@angular/router';
import {AuthService} from 'src/app/shared/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    @ViewChild('loginForm') loginForm: NgForm;
    emailModel = undefined;
    passwordModel = undefined;

    buttonDisabled = false;
    buttonState = '';

    constructor(private authService: AuthService, private notifications: NotificationsService, private router: Router) {
    }

    ngOnInit() {
    }

    onSubmit() {
        if (!this.loginForm.valid || this.buttonDisabled) {
            // Object.assign(this.loginForm, {languageCode: "en"})
            console.log(this.loginForm);
            return;
        }
        // Object.assign(this.loginForm.value, {languageCode: "en"})
        this.buttonDisabled = true;
        this.buttonState = 'show-spinner';
        this.authService.signIn(this.loginForm.value.email, this.loginForm.value.password).subscribe((user) => {
            this.router.navigate(['/app']);
        }, (error) => {
            this.buttonDisabled = false;
            this.buttonState = '';
            this.notifications.create('Error', error.error.message, NotificationType.Bare, {
                theClass: 'outline primary',
                timeOut: 6000,
                showProgressBar: false
            });
        });
    }
}
