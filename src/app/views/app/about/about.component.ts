import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationsService} from 'angular2-notifications';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../data/api.service';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
    aboutData: any;

    constructor(private http: HttpClient, private notifications: NotificationsService, private  api: ApiService) {
    }


    get facebook() {
        return this.aboutForm.get('facebook');
    }

    get instagram() {
        return this.aboutForm.get('instagram');
    }

    get linkedIn() {
        return this.aboutForm.get('linkedIn');
    }

    get aboutDetails() {
        return this.aboutForm.get('aboutDetails');
    }

    get email() {
        return this.aboutForm.get('email');
    }

    get phoneNo() {
        return this.aboutForm.get('phoneNo');
    }

    get address() {
        return this.aboutForm.get('address');
    }

    get twitter() {
        return this.aboutForm.get('twitter');
    }

    get pickart() {
        return this.aboutForm.get('pickart');
    }

    aboutForm = new FormGroup({
        facebook: new FormControl('', Validators.required),
        instagram: new FormControl('', Validators.required),
        linkedIn: new FormControl('', Validators.required),
        aboutDetails: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required),
        phoneNo: new FormControl('', Validators.required),
        address: new FormControl('', Validators.required),
        twitter: new FormControl('', Validators.required),
        pickart: new FormControl('', Validators.required),
    });

    ngOnInit(): void {
        this.getAboutData();
    }


    onsubmit() {
        if (this.aboutForm.invalid) {
            console.log('called');

            this.notifications.error('Error', 'Enter Data in Fields');
        } else {
            this.save();
        }
    }

    save() {
        this.api.addAbout(this.aboutForm.value).subscribe(data => {
            this.notifications.success('Data Updated Successfully', '');
        }, error => {
            this.notifications.error('Data is not Updated', error.error.message);
        });
    }

    getAboutData() {
        this.api.getAbout().subscribe(data => {
            this.aboutData = data.data[0];
            console.log(this.aboutData);
            this.aboutForm.setValue({
                facebook: this.aboutData.facebook,
                instagram: this.aboutData.instagram,
                linkedIn: this.aboutData.linkedIn,
                aboutDetails: this.aboutData.aboutDetails,
                email: this.aboutData.email,
                phoneNo: this.aboutData.phoneNo,
                address: this.aboutData.address,
                twitter: this.aboutData.twitter,
                pickart: this.aboutData.pickart,
            });
        }, error => {
            this.notifications.error('Competition Not Saved', error.error.message);
        });
    }

}
