import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {HttpClient} from '@angular/common/http';
import {NotificationsService} from 'angular2-notifications';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {GalleryService} from '../../gallery.service';
import {multicast} from 'rxjs/operators';

@Component({
    selector: 'app-add-image',
    templateUrl: './add-image.component.html',
    styleUrls: ['./add-image.component.scss']
})
export class AddImageComponent implements OnInit {
    @Output() itemSaved: EventEmitter<any> = new EventEmitter();
    myFile: any;
    showImgIcon = false;
    showDivIcon = true;
    imagevalidate = false;
    multipleFiles = [];

    constructor(private modalService: BsModalService, private api: GalleryService,
                private http: HttpClient,
                private notifications: NotificationsService) {
    }

    public addNewImage = new FormGroup({
        ImageGalleryTitleEn: new FormControl('', Validators.required),
        ImageGalleryTitleAr: new FormControl('', Validators.required),
        file: new FormControl(),
        FileToUpload: new FormControl('', [Validators.required])
    });

    get ImageGalleryTitleEn() {
        return this.addNewImage.get('ImageGalleryTitleEn');
    }

    get ImageGalleryTitleAr() {
        return this.addNewImage.get('ImageGalleryTitleAr');
    }

    get FileToUpload() {
        return this.addNewImage.get('FileToUpload');
    }

    myConfig = {
        url: 'https://',
        thumbnailWidth: 160,
        // tslint:disable-next-line: max-line-length
        previewTemplate: '<div class="dz-preview dz-file-preview mb-3">\n' +
            '\t<div class="d-flex flex-row ">\n' +
            '\t\t<div class="p-0 w-30 position-relative">\n' +
            '\t\t\t<div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon"></i></div>\n' +
            '\t\t</div>\n' +
            '\t\t<div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">\n' +
            '\t\t\t<div><span data-dz-name></span></div>\n' +
            '\t\t\t<div class="text-primary text-extra-small" data-dz-size />\n' +
            '\t\t</div>\n' +
            '\t</div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
    };

    file;
    modalRef: BsModalRef;
    config = {
        backdrop: true,
        ignoreBackdropClick: true,
        class: ''
    };

    @ViewChild('add_new_image', {static: true}) template: TemplateRef<any>;
    formData;

    onUploadError(event) {
        console.log(event);
    }

    onUploadSuccess(event) {
        console.log(event);
    }

    onMultipleFileSelect(event){
        this.multipleFiles.push(event);
        console.log(event);
        console.log(this.multipleFiles);
    }

    onDeleteMultipleImage(event){
        console.log(event);
        console.log(event.name);
        this.multipleFiles = this.multipleFiles.filter(x => x.name !== event.name);
        console.log(this.multipleFiles);
    }

    onFileSelect(event) {
        console.log("newFile");
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.myFile = event.target.files[0];
            if (this.myFile.size <= 5242880) {
                this.showDivIcon = false;
                this.showImgIcon = true;
                this.imagevalidate = false;
                console.log(false);
                this.addNewImage.patchValue({
                    FileToUpload: file
                });
            } else {
                this.imagevalidate = true;
            }


        }
    }

    deleteImageCall() {
        console.log('hello Delle');
    }

    show() {
        this.modalRef = this.modalService.show(this.template, this.config);
    }

    ngOnInit()
        :
        void {
    }


    onsubmit() {
        if (this.addNewImage.invalid) {
            this.notifications.error('Error', 'Enter Data in Fields');
        } else {
            this.save();
        }

    }

    save() {
        const file = this.addNewImage.get('FileToUpload').value;
        const formdata = new FormData();
        formdata.append('ImageGalleryTitleEn', this.addNewImage.get('ImageGalleryTitleEn').value);
        formdata.append('ImageGalleryTitleAr', this.addNewImage.get('ImageGalleryTitleAr').value);
        formdata.append('FileToUpload', file, file.name);
        console.log(formdata);
        this.api.addImages(formdata).subscribe(data => {
            this.notifications.success('Image Upload', '');
            this.itemSaved.emit();
            this.showImgIcon = false;
            this.showDivIcon = true;
            this.imagevalidate = false;
        }, error => {
            this.notifications.error('Image not uploaded', error.error.message);
        });
    }

    hide() {
        this.modalRef.hide();
        this.imagevalidate = false;
        this.addNewImage.reset();
    }

    deleteImage() {
        this.showDivIcon = true;
        this.imagevalidate = false;
        this.showImgIcon = false;
    }
}
