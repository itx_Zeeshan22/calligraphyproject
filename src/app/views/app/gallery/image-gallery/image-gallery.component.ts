import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ApiService} from '../../../../data/api.service';
import {HttpClient} from '@angular/common/http';
import {NotificationsService} from 'angular2-notifications';
import {TranslateService} from '@ngx-translate/core';
import {AddImageComponent} from './add-image/add-image.component';
import {GalleryService} from '../gallery.service';
import {error} from "@angular/compiler/src/util";

@Component({
    selector: 'app-image-gallery',
    templateUrl: './image-gallery.component.html',
    styleUrls: ['./image-gallery.component.scss']
})
export class ImageGalleryComponent implements OnInit {

    constructor(private appApisService: ApiService, private api: ApiService, private http: HttpClient,
                private notifications: NotificationsService, private translate: TranslateService) {
    }

    @ViewChild('linsh', {static: true}) addNewModalRef: AddImageComponent;
    currentPage = 0;
    itemsPerPage = 10;
    search = '';
    orderBy = '';
    totalItem = 0;
    settings = {
        singleSelection: false,
        idField: 'id',
        memberid: '0',
        textField: 'name',
        enableCheckAll: false,
        unSelectAllText: 'unselect',
        allowSearchFilter: true,
        limitSelection: -1,
        clearSearchFilter: true,
        maxHeight: 197,
        itemsShowLimit: 4,
        searchPlaceholderText: 'Search Category',
        noDataAvailablePlaceholderText: 'No Data Available',
        closeDropDownOnSelection: false,
        showSelectedItemsAtTop: true,
        defaultOpen: false
    };
    displayOptionsCollapsed = false;

    @Input() showOrderBy = true;
    @Input() showSearch = true;
    @Input() showItemsPerPage = true;
    @Input() showDisplayMode = true;
    @Input() displayMode = 'list';
    @Input() selectAllState = '';
    @Input() itemOptionsPerPage = [5, 10, 20];
    @Input() itemOrder = {label: 'Product Name', value: 'title'};
    @Input() itemOptionsOrders = [{label: 'Product Name', value: 'title'}, {
        label: 'Category',
        value: 'category'
    }, {label: 'Status', value: 'status'}];

    @Output() changeDisplayMode: EventEmitter<string> = new EventEmitter<string>();
    @Output() addNewItem: EventEmitter<any> = new EventEmitter();
    @Output() selectAllChange: EventEmitter<any> = new EventEmitter();
    @Output() searchKeyUp: EventEmitter<any> = new EventEmitter();
    @Output() itemsPerPageChange: EventEmitter<any> = new EventEmitter();
    @Output() changeOrderBy: EventEmitter<any> = new EventEmitter();
    images: any;
    files: File[] = [];
    multipleFiles = [];
    data = {
        categoryIds: [],
        fileToUpload: []
    }

    categoriesList: any;

    ngOnInit(): void {
        this.getAllImages();
        this.getCategories()
    }

    getAllImages() {
        this.api.getImages().subscribe((data) => {
            this.images = data.data;
            // this.totalItem = data.data.totalRecords;
            console.log(this.images);
        }, error => {
            console.log(error.message);
        });
    }


    delete(id) {
        console.log(id);
        this.api.deleteImage(id).subscribe(() => {
            this.getAllImages();
            this.notifications.success('Deleted Successfully');
        }, error => {
            this.notifications.error('error', error.message);
        });
    }

    isSelected(record: any) {
        // return this.selected.findIndex(x => x.id === record.id) > -1;
    }

    // onAddNewItem() {
    //     this.addNewModalRef.show();
    // }
    //
    // selectAll(event) {
    //     this.selectAllChange.emit(event);
    // }

    onChangeItemsPerPage(item) {
        // this.getAllImagesModel.paging = item;
        this.itemsPerPage = item;
        this.getAllImages();
        this.getCategories();
    }

    onContextMenuClick(action: string, item) {
        console.log('onContextMenuClick -> action :  ', action, ', item.id :', item.id);
        if (action === 'delete') {
            this.delete(item.id);
        }
    }


    // onMultipleFileSelect(event) {
    //     this.multipleFiles.push(event);
    //     console.log(event);
    //     console.log(this.multipleFiles);
    // }
    //
    // onDeleteMultipleImage(event) {
    //     console.log(event);
    //     console.log(event.name);
    //     this.multipleFiles = this.multipleFiles.filter(x => x.name !== event.name);
    //     console.log(this.multipleFiles);
    // }


    pageChanged(event: any): void {
        console.log(event.page);
        // this.getAllImagesModel.offset = event.page - 1;
        // this.getAllImagesModel.paging = this.itemsPerPage;
        this.getAllImages();
    }

    onSaved() {
        this.addNewModalRef.hide();
        this.getAllImages();
    }


    getCategories() {
        this.api.getCategory().subscribe(data => {
            this.categoriesList = data.data;
        }, error => {
            this.notifications.error('error', error.message);
        });
    }

    onFilterChange($event: any) {

    }

    onDropDownClose($event: any) {

    }


    onDeSelectCategory(event) {
        this.data.fileToUpload = this.data.fileToUpload.filter(data => data !== event.id);
        this.getAllImages()
    }

    onSelectAll($event: any) {

    }

    onDeSelectAll(event: any) {

    }

    onCategorySelect(event: any) {
        this.getImagesById(event.id);
        this.data.categoryIds.push(event.id);
    }

    getImagesById(id) {
        this.api.getByCategory(id).subscribe(data => {
            this.images = data.data;
        })
    }

    save() {
        console.log(this.data)
        this.api.saveImages(this.data).subscribe(data => {
            this.getAllImages();
        }, error => {
            console.log(error.message);
            this.notifications.error(error.error.message);
        })
    }


    onSelect(event) {
        this.data.fileToUpload.push(...event.addedFiles);
        console.log(this.data.fileToUpload)
        console.log(event)
        this.files.push(...event.addedFiles);

    }

    onRemove(event) {
        this.files.splice(this.files.indexOf(event), 1);
        this.data.fileToUpload.splice(this.multipleFiles.indexOf(event), 1);
        console.log(this.data)
    }
}

