import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery.component';
import { ImageGalleryComponent } from './image-gallery/image-gallery.component';
import { AddImageComponent } from './image-gallery/add-image/add-image.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {ContextMenuModule} from 'ngx-contextmenu';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {LayoutContainersModule} from '../../../containers/layout/layout.containers.module';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {DropzoneModule} from 'ngx-dropzone-wrapper';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import { UpdateCategoryComponent } from './category/update-category/update-category.component';
import {SelectDropDownModule} from 'ngx-select-dropdown';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import { CategoryComponent } from './category/category.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import {NgxDropzoneModule} from "ngx-dropzone";


@NgModule({
  declarations: [GalleryComponent, ImageGalleryComponent, AddImageComponent, UpdateCategoryComponent, CategoryComponent, AddCategoryComponent],
    imports: [
        CommonModule,
        GalleryRoutingModule,
        ReactiveFormsModule,
        TranslateModule,
        PaginationModule,
        FormsModule,
        ContextMenuModule,
        SelectDropDownModule,
        CollapseModule,
        NgxDropzoneModule,
        LayoutContainersModule,
        BsDropdownModule,
        DropzoneModule,
        ConfirmationPopoverModule,
        NgMultiSelectDropDownModule
    ]
})
export class GalleryModule { }
