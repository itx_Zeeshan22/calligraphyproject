import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GalleryComponent} from './gallery.component';
import {ImageGalleryComponent} from './image-gallery/image-gallery.component';
import {CategoryComponent} from './category/category.component';


const routes: Routes = [
    {
        path: '', component: GalleryComponent,
        children: [
            {path: '', redirectTo: 'image-gallery', pathMatch: 'full'},
            {path: 'image-gallery', component: ImageGalleryComponent},
            {path: 'category', component: CategoryComponent}
        ]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GalleryRoutingModule {
}
