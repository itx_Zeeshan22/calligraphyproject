import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class GalleryService {
    constructor(private http: HttpClient) {
    }

    uri = 'http://aqdarapi.timelineuae.com/';
    // uri = 'http://192.168.0.135/';

    lang = localStorage.getItem('__lang');

    addImages(formData): Observable<any> {
        return this.http.post(this.uri + 'api/ImageGallery/', formData);
    }

    getImages(formData): Observable<any> {
        return this.http.post(this.uri + 'api/ImageGallery/List', formData);
    }

    addVideo(formData): Observable<any> {
        return this.http.post(this.uri + 'api/VideoGallery/', formData);
    }

    getAllVideo(formData): Observable<any> {
        return this.http.post(this.uri + 'api/VideoGallery/List', formData);
    }

    update(id): Observable<any> {
        return this.http.put('/api/BeneficiaryPerson/Activate/${id}', id);
    }

    delete(id): Observable<any> {
        return this.http.delete(this.uri + `api/ImageGallery/Delete/${id}/`);
    }

    deleteVideo(id): Observable<any> {
        return this.http.delete(this.uri + `api/VideoGallery/Delete/${id}/`);
    }

    getBeneficiary(): Observable<any> {
        return this.http.get('/api/BeneficiaryPerson/{id}');
    }

    getAllBeneficiary(data): Observable<any> {
        return this.http.post<any>('http://aqdarapi.timelineuae.com/api/Apps/List', data);
    }

    addCategory(addNewCategory: any): Observable<any> {
        return this.http.post(this.uri + 'api/Category/Save', addNewCategory);

    }
}
