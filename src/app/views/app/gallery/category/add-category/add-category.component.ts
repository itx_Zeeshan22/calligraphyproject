import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ApiService} from '../../../../../data/api.service';
import {HttpClient} from '@angular/common/http';
import {AppComponent} from '../../../../../app.component';
import {NotificationsService} from 'angular2-notifications';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  @Output() itemSaved: EventEmitter<any> = new EventEmitter();
  myFile: any;
  showImgIcon = false;
  showDivIcon = true;
  imagevalidate = false;
  myconfig = {
    url: 'https://httpbin.org/post',
    thumbnailWidth: 160,
    // tslint:disable-next-line: max-line-length
    previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
  };

  constructor(private modalService: BsModalService, private api: ApiService,
              private http: HttpClient, private loadingspin: AppComponent,
              private notifications: NotificationsService) {
  }

  public addNewCategory = new FormGroup({
    name: new FormControl('', Validators.required),
  });

  get name() {
    return this.addNewCategory.get('FileToUpload');
  }

  private file;
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: ''
  };
  @ViewChild('add_new_video', {static: true}) template: TemplateRef<any>;
  formData;


  show() {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  ngOnInit(): void {
  }


  onsubmit() {
    if (this.addNewCategory.invalid) {
      this.notifications.error('Error', 'Enter Data in Fields');
    } else {
      this.save();
    }

  }


  save() {
    this.api.addCategory(this.addNewCategory.value).subscribe(data => {
      this.notifications.success('Category Added', '');
    }, error => {
      this.notifications.error('Video not uploaded', error.message);
    });
  }

  hide() {
    this.modalRef.hide();
    this.imagevalidate = false;
    this.addNewCategory.reset();
  }

}
