import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {ApiService} from '../../../../data/api.service';
import {HttpClient} from '@angular/common/http';
import {NotificationsService} from 'angular2-notifications';
import {UpdateCategoryComponent} from './update-category/update-category.component';
import {TranslateService} from '@ngx-translate/core';
import {AddCategoryComponent} from './add-category/add-category.component';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
    @ViewChild('linsh', {static: true}) addNewModalRef: AddCategoryComponent;
    @ViewChild('update', {static: true}) updateModalRef: UpdateCategoryComponent;
    currentPage = 0;
    itemsPerPage = 10;
    search = '';
    orderBy = '';
    totalItem = 0;
    displayOptionsCollapsed = false;

    @Input() showOrderBy = true;
    @Input() showSearch = true;
    @Input() showItemsPerPage = true;
    @Input() showDisplayMode = true;
    @Input() displayMode = 'list';
    @Input() selectAllState = '';
    @Input() itemOptionsPerPage = [5, 10, 20];
    @Input() itemOrder = {label: 'Product Name', value: 'title'};
    @Input() itemOptionsOrders = [{label: 'Product Name', value: 'title'}, {
        label: 'Category',
        value: 'category'
    }, {label: 'Status', value: 'status'}];

    @Output() changeDisplayMode: EventEmitter<string> = new EventEmitter<string>();
    @Output() addNewItem: EventEmitter<any> = new EventEmitter();
    @Output() selectAllChange: EventEmitter<any> = new EventEmitter();
    @Output() searchKeyUp: EventEmitter<any> = new EventEmitter();
    @Output() itemsPerPageChange: EventEmitter<any> = new EventEmitter();
    @Output() changeOrderBy: EventEmitter<any> = new EventEmitter();
    categories: any;
    cancelClicked: boolean;

    constructor(private appApisService: ApiService, private api: ApiService, private http: HttpClient,
                private notifications: NotificationsService, private translate: TranslateService) {
    }

    ngOnInit(): void {
        this.getAllCategories();
    }

    getAllCategories() {
        this.api.getCategory().subscribe(data => {
            this.categories = data.data;
            console.log(this.totalItem);
        }, error => {
            console.log(error.message);
        });
    }

    delete(id) {
        this.api.deleteCategory(id).subscribe(() => {
            this.getAllCategories();
            this.notifications.success('Deleted Successfully');
        }, error => {
            this.notifications.error('error', error.error.message);
        });
    }

    isSelected(record: any) {
        // return this.selected.findIndex(x => x.id === record.id) > -1;
    }

    onAddNewItem() {
        this.addNewModalRef.show();
    }

    selectAll(event) {
        // this.selectAllChange.emit(event);
    }

    onChangeItemsPerPage(item) {
        // this.getAllVideosModel.paging = item;
        // this.itemsPerPage = item;
        // this.getAllVideos();
    }

    onSearchKeyUp($event) {
        // this.getAllVideosModel.searchTerm = $event.target.value.toLowerCase().trim();
        // this.getAllVideos();
    }

    onContextMenuClick(action: string, item) {
        console.log('onContextMenuClick -> action :  ', action, ', item.id :', item.id);
        if (action === 'delete') {
            this.delete(item.id);
        }
    }

    onSelect(record: any) {

    }

    pageChanged(event: any): void {
        // console.log(event.page);
        // this.getAllVideosModel.offset = event.page - 1;
        // this.getAllVideosModel.paging = this.itemsPerPage;
        // this.getAllVideos();
    }

    OnSaved() {
        this.addNewModalRef.hide();
        this.getAllCategories();
    }

    edit(record: any) {
        console.log(record.id);
        this.updateModalRef.show();
    }
}
