import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing';
import {SharedModule} from 'src/app/shared/shared.module';
import {LayoutContainersModule} from 'src/app/containers/layout/layout.containers.module';
import {AccordionModule} from 'ngx-bootstrap/accordion';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ComponentsCardsModule} from '../../components/cards/components.cards.module';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ContextMenuModule} from 'ngx-contextmenu';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';

import {FileUploadModule} from 'ng2-file-upload';

import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {OWL_DATE_TIME_FORMATS, OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import httpInterceptProviders from '../../interceptor';
import {BsModalService} from 'ngx-bootstrap/modal';
import {AboutComponent} from './about/about.component';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';


export const MY_NATIVE_FORMATS = {
    fullPickerInput: {year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric'},
    datePickerInput: {year: 'numeric', month: 'numeric', day: 'numeric'},
    timePickerInput: {hour: 'numeric', minute: 'numeric'},
    monthYearLabel: {year: 'numeric', month: 'short'},
    dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
    monthYearA11yLabel: {year: 'numeric', month: 'long'},
};

@NgModule({
    declarations: [AppComponent,
        DashboardComponent,
        AboutComponent],
    imports: [
        CommonModule,
        AppRoutingModule,
        SharedModule,
        LayoutContainersModule,
        AccordionModule,
        NgMultiSelectDropDownModule.forRoot(),
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger',
        }),
        PaginationModule.forRoot(),
        SimpleNotificationsModule.forRoot({
            timeOut: 3000,
            showProgressBar: false
        }),
        ComponentsCardsModule,
        RoundProgressModule,
        ReactiveFormsModule,
        ContextMenuModule,
        FormsModule,
        CollapseModule,
        BsDropdownModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        FileUploadModule,
        ConfirmationPopoverModule.forRoot({
            focusButton: 'confirm',
        })

    ],
    providers: [
        DatePipe,
        {provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS},
        httpInterceptProviders,
        BsModalService
    ]
})
export class AppModule {
}

