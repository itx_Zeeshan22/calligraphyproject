import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AboutComponent} from './about/about.component';


const routes: Routes = [
    {
        path: '', component: AppComponent,
        children: [
            {
                path: '',
                component: DashboardComponent
            }, {
                path: 'about',
                component: AboutComponent
            },
            {
                path: 'gallery',
                loadChildren: () => import('./gallery/gallery.module').then(m => m.GalleryModule)
            }]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
