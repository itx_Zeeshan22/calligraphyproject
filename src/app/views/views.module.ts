import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {ViewsComponent} from './views.component';
import {ViewRoutingModule} from './views.routing';
import {SharedModule} from '../shared/shared.module';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireAuthGuardModule} from '@angular/fire/auth-guard';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {FileUploadModule} from 'ng2-file-upload';
import {OWL_DATE_TIME_FORMATS, OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import httpInterceptProviders from '../interceptor';
import {BsModalService} from 'ngx-bootstrap/modal';
export const MY_NATIVE_FORMATS = {
    fullPickerInput: {year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric'},
    datePickerInput: {year: 'numeric', month: 'numeric', day: 'numeric'},
    timePickerInput: {hour: 'numeric', minute: 'numeric'},
    monthYearLabel: {year: 'numeric', month: 'short'},
    dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
    monthYearA11yLabel: {year: 'numeric', month: 'long'},
};
@NgModule({
    declarations: [ViewsComponent],
    imports: [
        CommonModule,
        ViewRoutingModule,
        SharedModule,
        AngularFireAuthModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        AngularFireAuthGuardModule,
        SimpleNotificationsModule.forRoot({
                showProgressBar : false,
            }
        ),
        FileUploadModule
    ],
    providers: [
        DatePipe,
        {provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS},
        httpInterceptProviders,
        BsModalService
    ]
})
export class ViewsModule {
}
