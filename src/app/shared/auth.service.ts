import {Injectable} from '@angular/core';
import {Observable, from} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';


export interface ICreateCredentials {
    email: string;
    password: string;
    displayName: string;
}

export interface IPasswordReset {
    code: string;
    newPassword: string;
}

@Injectable({providedIn: 'root'})
export class AuthService {

    constructor(private router: Router, private http: HttpClient,
                private jwtHelper: JwtHelperService) {
    }

    public get loggedIn(): boolean {
        const token = localStorage.getItem('token');
        return !this.jwtHelper.isTokenExpired(token);
    }

    public static tokenGetter(): string {
        return localStorage.getItem('token');
    }

    //   login(username: string, password: string, languageCode: string): Observable<any> {
    //       return this.http.post<{ access: string }>('http://adsyc.timelinesystems.com/api/Login/SignIn', {
    //           username,
    //           password,
    //           languageCode
    //       }).pipe(tap(res => {
    //           console.log(res.data.token);
    //           localStorage.setItem('token', res.data.token);
    //       }));
    //   }
    //
    //   logout(): void {
    //       localStorage.removeItem('token');
    //       this.router.navigate(['/login']);
    //   }
    signIn(email: string, password: string): Observable<any> {
        return this.http.post<{ access: string }>('http://192.168.0.123:81/api/LoginAPI/Login', {
            email,
            password,
            // languageCode
        }).pipe(tap(res => {
            localStorage.setItem('token', res.data.token);
        }));
    }

    signOut(): void {
        localStorage.removeItem('token');
        this.router.navigate(['/user/login']);
    }

    register(credentials: ICreateCredentials) {
        // return from(
        //   this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password).then(
        //     () => {
        //       this.afAuth.auth.currentUser.updateProfile({displayName: credentials.displayName});
        //       this.afAuth.auth.updateCurrentUser(this.afAuth.auth.currentUser);
        //     }
        //   )
        // );
    }


    sendPasswordEmail(email) {
        // return from(this.afAuth.auth.sendPasswordResetEmail(email));
    }

    resetPassword(credentials: IPasswordReset) {
        // return from(this.afAuth.auth.confirmPasswordReset(credentials.code, credentials.newPassword));
    }

    //
    // get user(): firebase.User {
    //   return this.afAuth.auth.currentUser;
    // }

}
