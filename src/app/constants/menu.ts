export interface IMenuItem {
    id?: string;
    icon?: string;
    label: string;
    to: string;
    newWindow?: boolean;
    subs?: IMenuItem[];
}

const data: IMenuItem[] = [
    {
        icon: 'iconsminds-shop-4',
        label: 'Dashboard',
        to: '/app/',
    },
    {
        icon: 'iconsminds-bar-chart-4',
        label: 'about',
        to: '/app/about'
    },
    {
        icon: 'iconsminds-photo',
        // icon: 'icon-gallery',
        label: 'Gallery',
        to: '/app/gallery',
        subs: [
            {
                icon: 'iconsminds-video',
                label: 'Category',
                to: '/app/gallery/category'
            },
            {
                icon: 'iconsminds-photo',
                label: 'Image Gallery',
                to: '/app/gallery/image-gallery'
            }

        ]
    },

];
export default data;
