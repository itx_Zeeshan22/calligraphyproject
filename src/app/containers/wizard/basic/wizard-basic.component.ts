import {Component, OnInit} from '@angular/core';
import {Member} from "./basic";
import {MemberRegistrationService} from "../../../views/app/member-registration/member-registration.service";
import {NotificationsService} from "angular2-notifications";

@Component({
    selector: 'app-wizard-basic',
    templateUrl: './wizard-basic.component.html',
    styleUrls: ['./wizard-basic.component.css']
})
export class WizardBasicComponent implements OnInit {

    member: Member = {
        "id": 0,
        "memberNo": "string",
        "memberEmiratesId": "string",
        "memberNameEn": "string",
        "memberNameAr": "string",
        "memberNationality": "string",
        "memberDateOfBirth": "2020-06-25T13:36:15.490Z",
        "memberMobileNo": "string",
        "memberEmail": "string",
        "memberRegistrationDate": "2020-06-25T13:36:15.490Z",
        "uploadedPicture": 0,
        "uploadedSignature": 0,
        "beneficiaryPersonId": 0,
        "saveIncomplete": true,
        "lan": "string",
        "isActive": true,
    }

    // public form: {
    //     address: Address[];
    //     partner: CasePartnerList[];
    //     period: CasePeriodList[];
    // };

    constructor(private api: MemberRegistrationService, private notifications: NotificationsService) {
        //     this.form = {
        //         address: [],
        //         partner: [],
        //         period: []
        //     };
        //     this.addAddress();
    }

    ngOnInit() {
    }

    // public addAddress() {
    //     this.form.address.push({
    //         branchAddress: '',
    //         id: Date.now()
    //     });
    //     this.objection.caseBranchList = this.form.address;
    // }
    //
    // public addPartner() {
    //     this.form.partner.push({
    //         partnerName: '',
    //         id: Date.now()
    //     });
    //     this.objection.casePartnerList = this.form.partner;
    // }
    //
    // public addPeriod() {
    //     this.form.period.push({
    //         id: Date.now(),
    //         periodFrom: '',
    //         periodTo: '',
    //         tax: undefined,
    //         penalty: undefined,
    //         total: undefined,
    //     });
    //     this.objection.casePeriodList = this.form.period;
    // }
    //
    // public removeAddress(index: number): void {
    //     this.form.address.splice(index, 1);
    // }
    //
    // public removePartner(index: number): void {
    //     this.form.partner.splice(index, 1);
    // }
    //
    // public removePeriod(index: number): void {
    //     this.form.period.splice(index, 1);
    // }

    onSubmit() {
        console.log(this.member);
        console.log('hahahahahahhahahahahah')
        this.api.create(this.member).subscribe(data => {
            this.notifications.success('data submitted')
        }, error => {
            console.log(error);
        });
    }
}
